﻿namespace SpectrumPanoramaControl
{
    public struct BandRange
    {
        public double Start { get; }
        public double End { get; }

        public BandRange(double start, double end) 
        {
            Start = start;
            End = end;
        }
    }
}
