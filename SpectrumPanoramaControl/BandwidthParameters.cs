﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpectrumPanoramaControl
{
    public class BandwidthParameters
    {
        public double BandWidth;
        public int BandWidthCount;
        public int DotsPerBand;

        public BandwidthParameters(double BandWidth, int BandWidthCount, int DotsPerBand)
        {
            this.BandWidth = BandWidth;
            this.BandWidthCount = BandWidthCount;
            this.DotsPerBand = DotsPerBand;
        }
    }
}
